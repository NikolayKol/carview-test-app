package kolomeytsev.test.carview.navigators

import kolomeytsev.test.carview.models.Dot
import kolomeytsev.test.carview.models.Position
import kotlin.math.abs
import kotlin.math.atan
import kotlin.math.pow
import kotlin.math.sqrt

private const val LINEAR_VELOCITY = 200 //px to second
private const val ANGLE_VELOCITY = 180 //degree to second

/**
 * реализация навигатора для решения А
 */
class NavigatorA : Navigator {

    private var rotateToDistance: Float = 0f

    private var fromX: Float = 0f
    private var fromY: Float = 0f

    private var toX: Float = 0f
    private var toY: Float = 0f

    private var fromAngle = 0f
    private var toAngle = 0f

    override fun navigate(from: Position, to: Dot): Long {
        fromX = from.x
        fromY = from.y

        toX = to.x
        toY = to.y

        fromAngle = from.angle
        toAngle = calculateToAngle(from, to)

        val rotateDuration = (abs(toAngle - fromAngle) / ANGLE_VELOCITY) * 1000
        val distanceDuration = calculateDistanceDuration(from, to) * 1000

        rotateToDistance = rotateDuration / (rotateDuration + distanceDuration)

        return (rotateDuration + distanceDuration).toLong()
    }

    override fun updatePosition(out: Position, pathPart: Float) {
        if (pathPart > 1 || pathPart < 0) {
            throw IllegalArgumentException()
        }

        when {
            rotateToDistance.equals(Float.NaN) -> { //направляемся в ту же точку из которой выехали
                out.x = toX
                out.y = toY
                out.angle = toAngle % 360
            }
            pathPart <= rotateToDistance -> {
                val partOfRotate = pathPart / rotateToDistance
                val partOfDegrees = (toAngle - fromAngle) * partOfRotate

                out.angle = (fromAngle + partOfDegrees) % 360
            }
            else -> {
                val partOfDistance = (pathPart - rotateToDistance) / (1 - rotateToDistance)

                out.x = fromX + (partOfDistance * (toX - fromX))
                out.y = fromY + (partOfDistance * (toY - fromY))
                out.angle = toAngle % 360
            }
        }
    }

    private fun calculateDistanceDuration(from: Position, to: Dot): Float {
        val distance = sqrt((to.x - from.x).pow(2) + (to.y - from.y).pow(2))
        return distance / LINEAR_VELOCITY
    }

    private fun calculateToAngle(from: Position, to: Dot): Float {
        return if (from.x == to.x && from.y == to.y) {
            from.angle
        } else {
            var lineAngle = Math.toDegrees(atan((to.y - from.y) / (to.x - from.x)).toDouble()) + 90

            if (to.x < from.x) {
                lineAngle += 180
            }

            if (abs(lineAngle - fromAngle) > 180) {
                if (lineAngle < fromAngle) {
                    lineAngle += 360
                } else {
                    fromAngle += 360
                }
            }

            lineAngle.toFloat()
        }
    }
}