package kolomeytsev.test.carview.navigators

import kolomeytsev.test.carview.models.Dot
import kolomeytsev.test.carview.models.Position

/**
 * Интерфейс для расчета траетктории и угла поворота
 */
interface Navigator {

    /**
     * вызывается для построения маршрута
     * @param from - текущая позиция машины
     * @param to - точка назначения
     * @return время в пути
     */
    fun navigate(from: Position, to: Dot): Long

    /**
     * метод для получения текущего расположения машины в зависимости от пройденного пути
     * @param out - позиция машины, новое расположение запишется сюда
     * @param pathPart - доля пройденного пути, значение может быть от 0 до 1
     */
    fun updatePosition(out: Position, pathPart: Float)
}