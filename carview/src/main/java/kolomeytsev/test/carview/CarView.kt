package kolomeytsev.test.carview

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.animation.addListener
import kolomeytsev.test.carview.models.Car
import kolomeytsev.test.carview.models.Dot
import kolomeytsev.test.carview.models.Position
import kolomeytsev.test.carview.navigators.NavigatorA

/**
 * View для анимированной отрисовки перемещения машины
 */
class CarView
@JvmOverloads
constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : View(context, attrs, defStyle) {

    private val car: Car
    private var targetDot: Dot? = null
    private val dots = mutableListOf<Dot>()

    private val dotRadius: Float

    private val carPaint = Paint()
    private val dotPaint = Paint()

    init {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.CarView)

        dotRadius = ta
            .getDimension(R.styleable.CarView_target_radius, context.resources.displayMetrics.density * 2)

        dotPaint.color = ta.getColor(R.styleable.CarView_target_color, Color.BLUE)
        carPaint.color = ta.getColor(R.styleable.CarView_car_color, Color.YELLOW)

        val carWidth = ta
            .getDimension(R.styleable.CarView_car_width, context.resources.displayMetrics.density * 24)
        val carHeight = ta
            .getDimension(R.styleable.CarView_car_height, context.resources.displayMetrics.density * 48)

        ta.recycle()

        car = Car(
            Pair(carWidth, carHeight),
            Position(),
            NavigatorA()
        )
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        gestureDetector.onTouchEvent(event)
        return true
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        if (oldw == 0 && oldh == 0) {
            car.position.apply {
                x = (w / 2).toFloat()
                y = (h / 2).toFloat()
                angle = 0f
            }
        }
    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawColor(Color.WHITE)

        targetDot?.let { drawDot(it, canvas) }

        dots.forEach { drawDot(it, canvas) }

        drawCar(canvas)
    }

    private fun startAnimation() {
        val pathDuration = car.navigator.navigate(car.position, targetDot!!)

        ValueAnimator
            .ofFloat(0f, 1f)
            .apply {
                duration = pathDuration
                interpolator = AccelerateDecelerateInterpolator()
                addUpdateListener {
                    car.navigator.updatePosition(car.position, it.animatedValue as Float)
                    invalidate()
                }
                addListener(onEnd = { endAnimation() })
            }
            .start()
    }

    private fun endAnimation() {
        targetDot = null
        if (dots.isNotEmpty()) {
            targetDot = dots[0]
            dots.removeAt(0)
            startAnimation()
        }
    }

    private fun drawCar(canvas: Canvas) {
        val left = car.position.x - (car.size.first / 2)
        val top = car.position.y - (car.size.second / 2)

        canvas.save()
        canvas.rotate(car.position.angle, car.position.x, car.position.y)
        canvas.drawRect(left, top, left + car.size.first, top + car.size.second, carPaint)
        canvas.restore()
    }

    private fun drawDot(dot: Dot, canvas: Canvas) {
        canvas.drawCircle(dot.x, dot.y, dotRadius, dotPaint)
    }

    private val gestureDetector = GestureDetector(
        context,
        object : GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapUp(e: MotionEvent): Boolean {
                if (targetDot == null) {
                    targetDot = Dot(e.x, e.y)
                    startAnimation()
                } else {
                    dots.add(Dot(e.x, e.y))
                }
                invalidate()
                return true
            }
        }
    )
}