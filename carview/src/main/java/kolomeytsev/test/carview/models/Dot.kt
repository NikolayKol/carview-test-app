package kolomeytsev.test.carview.models

/**
 * класс описывающий модель точки назначения
 */
data class Dot(val x: Float, val y: Float)