package kolomeytsev.test.carview.models

import kolomeytsev.test.carview.navigators.Navigator

/**
 * класс описывыающий модель машины
 * @property size - размеры в пикселях ширина, высота
 * @property position - текущее положение машины
 * @property navigator - реализация навигатора
 */
data class Car(
    val size: Pair<Float, Float> = Pair(0f, 0f),
    val position: Position = Position(),
    val navigator: Navigator
)