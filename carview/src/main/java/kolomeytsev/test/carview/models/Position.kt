package kolomeytsev.test.carview.models


/**
 * класс описывающий расположение машины, координаты и угол поворота
 */
data class Position(
    var x: Float = 0f,
    var y: Float = 0f,
    var angle: Float = 0f
)